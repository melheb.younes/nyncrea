<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = ['https://i.ibb.co/wKrG0Mx/image1.jpg','https://i.ibb.co/gV3Rtr4/image0.jpg','https://i.ibb.co/N9cD7CM/image2.jpg','https://i.ibb.co/FJYxbzc/image3.jpg'];
        return [
            'title' => $this->faker->sentence(4),
            'slug' => $this->faker->slug,
            'subtitle' => $this->faker->sentence(5),
            'description' => $this->faker->text(),
            'price' => $this->faker->numberBetween(15,300) * 100,
            'image' => Arr::random($image)
        ];
    }
}
