<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Attache tétine filles',
            'slug' => 'attache-tetine-filles'
        ]);
        Category::create([
            'name' => 'Attache tétine garçons',
            'slug' => 'attache-tetine-garçons'
        ]);
        Category::create([
            'name' => 'Attache tétine neutre',
            'slug' => 'attache-tetine-neutre'
        ]);
        Category::create([
            'name' => 'Accessoires',
            'slug' => 'accessoires'
        ]);
        Category::create([
            'name' => 'Anneau de dentition',
            'slug' => 'attache-de-dentition'
        ]);
        Category::create([
            'name' => 'Attache eveil',
            'slug' => 'attache-eveil'
        ]);
    }
}
