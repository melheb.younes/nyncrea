<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Database\Seeders\CategoriesTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);

        $categories = Category::all();

        // \App\Models\User::factory(10)->create();
        \App\Models\Product::factory(30)->create()->each(function ($product) use ($categories) { 
            $product->categories()->attach(
                $categories->random(rand(1, 6))->pluck('id')->toArray()
            ); 
        });

    }
}
