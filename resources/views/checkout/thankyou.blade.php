@extends('layouts.app')

@section('content')
    <div class="py-12 h-screen">
        <div class="max-w-md mx-auto bg-white rounded-xl shadow-xl overflow-hidden md:max-w-md">
            <div class="md:flex">
                <div class="w-full p-3 py-10">
                    <div class="flex justify-center"> <img src="https://i.imgur.com/QOi7Nie.png" width="80"> </div>
                    <div class="flex justify-center mt-3"> <span class="text-xl font-medium">Votre commande à bien été traitée avec succès.</span> </div>
                    <div class="px-14 mt-5"> <a href="{{ route('home') }}"><button class="h-12 bg-gray-800 w-full text-white text-md rounded hover:shadow hover:bg-gray-700">Retour à l'accueil</button> </a></div>
                </div>
            </div>
        </div>
    </div>
@endsection