@extends('layouts.app')

@section('extra-meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('extra-script')
    <script src="https://js.stripe.com/v3/"></script>
@endsection

@section('content')
    <h1 class="text-3xl text-center font-extrabold mt-5 text-black">Paiement</h1>
    <div class="w-full max-w-2xl mx-auto my-24 shadow-2xl container p-20">
        <form id="payment-form" action="{{ route('checkout.store') }}" method="POST" >
            @csrf
            <div id="card-element" class="shadow-xl">
            <!-- Elements will create input elements here -->
            </div>
            <!-- We'll put the error messages in this element -->
            <div id="card-errors" role="alert"></div> 
            <button class="flex justify-center mx-auto w-50 px-10 py-3 mt-20 font-medium text-white uppercase bg-gray-800 rounded-full shadow item-center hover:bg-gray-700 focus:shadow-outline focus:outline-none" id="submit" type="submit">
                <svg id="svgHide" class="invisible animate-spin text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                    <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                </svg>
                Payer ( {{ getPrice(Cart::total()) }} )
            </button>
        </form>
    </div>
@endsection

@section('extra-js')
    <script>
        var stripe = Stripe('pk_test_51IFyhnIF7lROwZJdJauVno7X5uZgkr0KWewKLgFXFCfYvEBXBWaxQfWpQV37Lh5BPRY5uodLLyI8ndI04t0OwpgX00SCs3neUc');
        var elements = stripe.elements();

        var style = {
            base: {
                color: "#32325d",
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                    "::placeholder": {
                        color: "#aab7c4"
                    }
            },
            invalid: {
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        };

        var card = elements.create("card", { style: style });
        card.mount("#card-element");

        card.on('change', ({error}) => {
            let displayError = document.getElementById('card-errors');
            if (error) {
                displayError.classList.add('text-red-400', 'italic', 'mt-4');
                displayError.textContent = error.message;
            } else {
                displayError.textContent = '';
            }
        });

        var form = document.getElementById('payment-form');
        var submitButton = document.getElementById('submit');
        var svgLoading = document.getElementById('svgHide');

        form.addEventListener('submit', function(ev) {
            ev.preventDefault();
            submitButton.disabled = true;
            svgLoading.classList.add('-ml-1','mr-3','h-5', 'w-5');
            svgLoading.classList.remove('invisible');
            stripe.confirmCardPayment("{{ $clientSecret }}", {
                payment_method: {
                    card: card
                }
            }).then(function(result) {
                if (result.error) {
                    // Show error to your customer (e.g., insufficient funds)
                    console.log(result.error.message);
                    submitButton.disabled = false;
                    svgLoading.classList.add('invisible');
                    svgLoading.classList.remove('-ml-1','mr-3','h-5', 'w-5');
                } else {
                    // The payment has been processed!
                    if (result.paymentIntent.status === 'succeeded') {
                        var paymentIntent = result.paymentIntent;
                        var url = form.action;
                        var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

                        axios.post(url, {
                            paymentIntent: paymentIntent
                        })
                        .then(function (data) {
                            if (data.status === 400) {
                                var redirect = '/';
                            } else {
                                var redirect = '/thank-you';
                            }
                            // console.log(data)
                            window.location.href = redirect;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    }
                }
            });
        });
    </script>
@endsection