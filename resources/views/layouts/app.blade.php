
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Nyn Crea</title>
        
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        @yield('extra-meta')

        @yield('extra-script')

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
    </head>
    <body class="bg-white text-gray-600 work-sans leading-normal text-base tracking-normal">
        
        @include('layouts.navbar.nav')
        
        <main>
            @include('layouts.sessions.success')
            
            @include('layouts.sessions.danger')

            @include('layouts.sessions.error')

            @yield('content')
        </main>
        
        @include('layouts.footers.footer')
        
        @yield('extra-js')
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>