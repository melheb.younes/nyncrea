<form action="{{ route('products.search') }}" class="">
    <input type="text" name="q" placeholder="Rechercher" value="{{ request()->q ?? "" }}" class="border-solid border-2 border-light-gray-500 mr-2 px-2 py-2 placeholder-gray-400 text-gray-700 relative bg-white rounded text-sm shadow outline-none focus:outline-none focus:border-blue-300 focus:shadow-outline inline-block no-underline hover:text-black">
    <button class="inline-block text-gray-500 hover:text-black mr-5" type="submit">
        Chercher
    </button>
</form>