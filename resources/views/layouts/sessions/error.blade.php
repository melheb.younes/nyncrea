@if(session('error'))
    <div>
        {{ session('error') }}
    </div>
@endif

@if(count($errors) > 0)
    <div class="container mx-auto text-white px-6 py-4 border-0 rounded relative mb-4 bg-red-300">
        <ul>
            @foreach ($errors->all() as $error)
                <li class="inline-block align-middle mr-8">
                    <b>{{$error}}</b> 
                </li>
            @endforeach
        </ul>
    </div>
@endif