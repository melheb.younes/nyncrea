@extends('layouts.app')

@section('content')
<main class="my-8">
    <div class="container mx-auto px-6">
        <div class="md:flex md:items-center">
            <div class="flex w-full h-64 md:w-1/2 lg:h-96 mb-40 lg:mb-0">
                <div>
                    @if($product->images)
                        <img class="cursor-pointer thumbnail rounded-md mb-5" width="60" src="{{ asset('storage/' . $product->image) }}" alt="img des produits">
                        @foreach (json_decode($product->images, true) as $image)
                            <img class="cursor-pointer thumbnail rounded-md mb-5" width="60" src="{{ asset('storage/' . $image) }}" alt="img de produit">
                        @endforeach
                    @endif
                </div>
                <img id="mainImage" class="h-full rounded-md object-cover max-w-lg mx-auto" src="{{ asset('storage/' . $product->image) }}" alt="img des produits">
            </div>
            <div class="w-full max-w-lg mx-auto mt-5 md:ml-8 md:mt-0 pt-8 md:w-1/2">
                <h3 class="text-gray-700 uppercase text-lg">{{ $product->title }}</h3>
                <span class="text-gray-500 mt-3">{{ $product->getPriceEuro() }}</span>
                @if($stock === 'Produit indisponible')
                <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 bg-red-600 rounded-full">{{ $stock }}</span>
                @endif
                <hr class="my-3">
                <div class="mt-2">
                    <label class="text-gray-700 text-sm" for="count">Description:</label>
                    <div class="flex items-center mt-1">
                        <p class="text-gray-700 text-sm">{!! $product->description !!}</p>
                    </div>
                </div>
                {{-- <div class="mt-3">
                    <label class="text-gray-700 text-sm" for="count">Color:</label>
                    <div class="flex items-center mt-1">
                        <button class="h-5 w-5 rounded-full bg-blue-600 border-2 border-blue-200 mr-2 focus:outline-none"></button>
                        <button class="h-5 w-5 rounded-full bg-black-600 mr-2 focus:outline-none"></button>
                        <button class="h-5 w-5 rounded-full bg-pink-600 mr-2 focus:outline-none"></button>
                    </div>
                </div> --}}
                <div class="flex items-center mt-6">
                    {{-- <button class="px-8 py-2 bg-indigo-600 text-white text-sm font-medium rounded hover:bg-indigo-500 focus:outline-none focus:bg-indigo-500">Commander</button> --}}
                    @if($stock === '')
                        <form action="{{ route('cart.store') }}" method="post">
                            @csrf
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <button type="submit" class="mx-2 text-gray-600 border rounded-md p-2 hover:bg-gray-200 focus:outline-none">
                                <svg class="h-5 w-5" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path></svg>
                            </button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
        <div class="pt-20">
            <h3 class="text-gray-600 text-2xl font-medium">Plus de produits</h3>
            <div class="grid gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 mt-6">
                @foreach ($products as $oneProduct)
                    <a href="{{ route('products.show', $oneProduct->slug) }}">
                        <div class="w-full max-w-sm mx-auto rounded-md shadow-md overflow-hidden">
                            <div class="flex items-end justify-end h-72 w-full bg-cover" style="background-image: url('{{ asset('storage/' . $oneProduct->image) }}')">
                                @if($stock === '')
                                    <form action="{{ route('cart.store') }}" method="post" class="flex items-end justify-end h-40 w-full bg-cover">
                                        @csrf
                                        <input type="hidden" name="product_id" value="{{ $oneProduct->id }}">
                                        <button class="p-2 rounded-full bg-blue-600 text-white mx-5 -mb-4 hover:bg-blue-500 focus:outline-none focus:bg-blue-500">
                                            <svg class="h-5 w-5" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path></svg>
                                        </button>
                                    </form>
                                @endif
                                
                            </div>
                            <div class="px-5 py-3">
                                <h3 class="text-gray-700 uppercase">{{ $oneProduct->title }}</h3>
                                <span class="text-gray-500 mt-2">{{ $oneProduct->getPriceEuro() }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</main>
@endsection

@section('extra-js')
    <script>
        var mainImage = document.querySelector('#mainImage');
        var thumbnails = document.querySelectorAll('.thumbnail');

        thumbnails.forEach((element) => element.addEventListener('click', changeImage));
        // thumbnails.forEach(element => {
        //     element.addEventListener('click', changeImage);
        // });

        function changeImage(e) {
            mainImage.src = this.src;
        }
    </script>
@endsection