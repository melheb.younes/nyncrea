@extends('layouts.app')

@section('content')
  
    <h1 class="text-3xl text-center font-extrabold mt-5 text-black">La boutique Nyn créa</h1>

    @include('layouts.navbar.navCategory')

    <div class="container mx-auto rounded-xl p-3 text-black font-bold text-center bg-gray-100">
        @if(request()->input('q'))
            <p>{{ $products->total() }} résultat(s) pour la recherche "{{ request()->q }}"</p>
        @endif  
    </div>

    @include('layouts.shop.shop')
    {{ $products->appends(request()->input())->links() }}
@endsection