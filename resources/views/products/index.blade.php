@extends('layouts.app')

@section('content')
  
  <h1 class="text-3xl text-center font-extrabold mt-5 text-black">La boutique Nyn créa</h1>

  @include('layouts.navbar.navCategory')
    
  @include('layouts.shop.shop')
  {{ $products->appends(request()->input())->links() }}
@endsection