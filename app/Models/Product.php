<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['stock'];

    /**
     * Get formated price euro for the shopping cart
     *
     * @return int
     */
    public function getPriceEuroForCart(){
        
        $price = $this->price / 100;

        return number_format($price, 2, '.', ' ');
    }

    /**
     * Get formated price euro 
     *
     * @return int
     */
    public function getPriceEuro(){
        
        $price = $this->price / 100;

        return number_format($price, 2, ',', ' ') . ' €';
    }

    /**
     * Get a serialize array of products for the order 
     *
     * @return int
     */
    public function getProduct(){
        
        $products = [];
        $i = 0;
        
        foreach (Cart::content() as $product) {
            $products['product_' . $i][] = $product->model->title;
            $products['product_' . $i][] = $product->model->price;
            $products['product_' . $i][] = $product->model->qty;
            $i++;
        }

        return serialize($products);
    }

    /**
     * The categories that belong to the product.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
