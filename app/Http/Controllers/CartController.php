<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart.cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Search if we have a duplicata in the cart  
        $duplicata = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id == $request->product_id;
        });

        // If we have a duplicata we make a redirection with a flash message 
        if ($duplicata->isNotEmpty()) {
            return redirect()->route('products.index')->with('danger', 'Le produit est déja dans le panier.');
        }

        // Find the selected product with his id 
        $product = Product::find($request->product_id);
        
        // Add the product in the cart & associate it with the Product model
        Cart::add($product->id, $product->title, 1, $product->getPriceEuroForCart(),['size' => 'large'], 20)
            ->associate('App\Models\Product');

        return redirect()->route('products.index')->with('success', 'Le produit a bien été ajouté.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rowId)
    {
        // stock data on json in $data
        $data = $request->json()->all();

        // Validate the qty of cart
        $validate = Validator::make($request->all(), [
            'qty' => 'required|numeric|between:1,5'
        ]);
        
        // message if validate is fail
        if ($validate->fails()) {
            Session::flash('danger', 'La quantité du produit ne doit pas dépasser 5');

            return response()->json(['error' => 'Cart Quantity Has Been Not Updated.']);
        }

        // verifiy  stock
        if ($data['qty'] > $data['stock']){
            Session::flash('danger', 'La quantité du produit n\'est pas disponible');

            return response()->json(['error' => 'Product Quantity Not Available']);
        }
        // Update the cart with the new qty
        Cart::update($rowId, $data['qty']);

        // Message if validate success
        Session::flash('success', 'La quantité du produit est passé à ' . $data['qty']);

        return response()->json(['success' => 'Cart Quantity Has Been Updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId)
    {
        // Remove a product
        Cart::remove($rowId);

        return back()->with('success', 'Le produit a été supprimer du panier');
    }
}
