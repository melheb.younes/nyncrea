<?php

namespace App\Http\Controllers;

use DateTime;
use Stripe\Stripe;
use App\Models\Order;
use App\Models\Product;
use Stripe\PaymentIntent;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Redirect to shop if cart is empty
        if (Cart::count() <= 0) {
            return redirect()->route('products.index');
        }

        // Set the api key
        Stripe::setApiKey('sk_test_51IFyhnIF7lROwZJdSCZrf7PgA6EzTQaSp33UTw7xsgJ4jWiZSYOzLFlNVr2Qjmr3RmUnCJq3kGt4WzSfkLicLYoG005bCVMiMK');
        
        // Get the total price and make it decimal
        $amount = Cart::total();
        $amount = (Int) $amount;
        $amount *= 100; 
        

        // Create a intnt payment
        $intent = PaymentIntent::create([
            'amount' => $amount,
            'currency' => 'eur',
            // Verify your integration in this guide by including this parameter
            // 'metadata' => [
            //     'integration_check' => 'accept_a_payment',
            //     'userId' => 15
            // ],
        ]);
        
        // Get the client secret
        $clientSecret = Arr::get($intent, 'client_secret');

        return view('checkout.index', compact('clientSecret'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->checkIfNotAvailable()) {
            Session::flash('danger', 'Un produit dans votre panier n\'est plus disponible.');

            return response()->json(['success' => false], 400);
        }
        // Get the request JSon from view
        $data = $request->json()->all();
        
        // store the new order
        $order = new Order();

        $order->payment_intent_id = $data['paymentIntent']['id'];

        $order->amount = $data['paymentIntent']['amount'];
        
        $order->payment_created_at = (new DateTime())
                                    ->setTimestamp($data['paymentIntent']['created'])
                                    ->format('Y-m-d H:i:s');
        
        // Get the products from the Product model
        $products = new Product;

        $order->product = $products->getProduct();

        $order->user_id = 15;

        $order->save();

        if ($data['paymentIntent']['status'] === 'succeeded') {
            $this->updateStock();
            Cart::destroy();
            Session::flash('success', 'Votre commande à bien été traitée avec succès.');
            return response()->json(['success' => 'Payment Intent Succeeded']);
        } else {
            return response()->json(['error' => 'Payment Intent NOT Succeeded']);
        }
    }

    /**
     * Reditect to page thank you
     *
     * @return void
     */
    public function thankyou()
    {
        return session::has('success') ? view('checkout.thankyou') : redirect()->route('products.index');
    }


    /**
     * Check if the product not available
     *
     * @return boolean
     */
    private function checkIfNotAvailable() 
    {
        foreach (Cart::content() as $item) {
            $product = Product::find($item->model->id);

            if ($product->stock < $item->qty) {
                return true;
            }
        }

        return false;
    }


    /**
     * Update stock 
     *
     * @return void
     */
    private function updateStock() 
    {
        foreach (Cart::content() as $item) {
            $product = Product::find($item->model->id);

            $product->update(['stock' => $product->stock - $item->qty]);
        }
    }

}
