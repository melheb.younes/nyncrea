<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->category) {
            $products = Product::with('categories')->whereHas('categories', function($query) {
                $query->where('slug', request()->category);
            })->orderBy('created_at', 'DESC')->paginate(8);
        }else {
            $products = Product::with('categories')->orderBy('created_at', 'DESC')->paginate(8);
        }

        $categories = Category::all();
        
        return view('products.index', compact('products','categories'));
    }

    /**
     * Display a listing of the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $products = Product::inRandomOrder()->take(4)->get();
        return view('welcome', compact('products'));
    }


    /**
     * Display the specified product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $products = Product::inRandomOrder()->take(4)->get();

        $product = Product::where('slug', $slug)->firstOrFail();

        $stock = $product->stock == 0 ? 'Produit indisponible' : '';

        return view('products.show', compact('product','products', 'stock'));
    }

    /**
     * Display the search products.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        request()->validate([
            'q' => 'required|min:3'
        ]);

        $q = request()->input('q');
        
        $products = Product::where('title', 'like', "%$q%")
                            ->orWhere('description', 'like', "%$q%")
                            ->paginate(8);

        $categories = Category::all();

        return view('products.search', compact('products','categories'));
    }

}
