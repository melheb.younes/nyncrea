<?php

function getPrice($priceInFortmatGlob)
{
    $price = floatval($priceInFortmatGlob);

    return number_format($price, 2, ',', ' ') . ' €';
}